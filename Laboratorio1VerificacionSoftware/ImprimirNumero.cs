﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Numeros
{
	public class Laboratorio1
	{
		public Laboratorio1() { }

		public string ImprimirNumeros()
		{
			string resultado = string.Empty;
			for (int i = 1; i < 10; i++)
			{
				resultado += i.ToString();
			}
			return resultado;
		}
		public string ImprimirNumeros(int numeroTope)
		{
			string resultado = string.Empty;
			for (int i = 1; i < numeroTope; i++) 
			{
				resultado += i.ToString();
			}		
			return resultado;
		}
	}
}
