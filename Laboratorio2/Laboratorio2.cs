﻿namespace Letras
{
	public class Laboratorio2
	{
		public string encuentraLaLetra(char letra, string str)
		{
			string msj = "";
			for (int i = 0; i < str.Length; i++) 
			{
				if (str[i] == letra)
				{
					msj = $"La letra está en el index: {i}";
					break;
				}
			}
			return msj;
		}
	}
}