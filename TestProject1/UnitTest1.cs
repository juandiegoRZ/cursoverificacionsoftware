using Letras;
using Numeros;

namespace TestLaboratorio
{
	public class Tests
	{
		[Test]
		public void ImprimirNumerosSinParametros()
		{
			var prueba = new Laboratorio1();
			string resultado = prueba.ImprimirNumeros();
			Assert.That(resultado, Is.EqualTo("123456789"));
		}
		[Test]
		public void ImprimirNumerosConParametros()
		{
			var prueba2 = new Laboratorio1();
			string resultado = prueba2.ImprimirNumeros(15);
			Assert.That(resultado, Is.EqualTo("1234567891011121314"));
		}
		[Test]
		public void EcontrarLetra()
		{
			var pruebaLaboratorio2 = new Laboratorio2();
			string resultado = pruebaLaboratorio2.encuentraLaLetra('p', "Esto es un prueba");
			Assert.That(resultado, Is.EqualTo($"La letra est� en el index: 11"));
		}
	}
}